import meshio;
import numpy as np;

def ply2obj():
    mesh = meshio.read('./input/aircraft.stl');
    mesh.write('./output/aircraft_stl_to_obj.obj', file_format='obj');

def getVert(vertices, faces):
    nVerts = [];

    for f in faces:
        [v1, v2, v3] = f;
        [x1, y1, z1] = vertices[v1];
        [x2, y2, z2] = vertices[v2];
        [x3, y3, z3] = vertices[v3];
        
        p1 = np.array([x1, y1, z1]);
        p2 = np.array([x2, y2, z2]);
        p3 = np.array([x3, y3, z3]);

        n = np.cross(p1 - p2, p2 - p3);
        l = np.dot(n, n) ** 0.5;
        nVerts.append((n / l).tolist());
        nVerts.append(p1);
        nVerts.append(p2);
        nVerts.append(p3);

    return nVerts;

def getFace(faces):
    nFaces = [];

    for f in faces:
        [v1, v2, v3] = f;
        p = np.array([v1 * 0.1, v2 * 0.1, v3 * 0.1]);
        nFaces.append(p);

    return nFaces;

if __name__ == '__main__':
    ply2obj();

    mesh: object = meshio.read('./output/aircraft_stl_to_obj.obj');

    vertices: list = mesh.points;
    faces = mesh.cells[0].data;

    nVerts = vertices * 0.001;
    nFaces = faces;

    cells = [
        ("triangle", nFaces),
    ]

    mesh2 = meshio.Mesh(nVerts, cells);

    mesh2.write('./output/aircraft_compression.obj', file_format='obj')